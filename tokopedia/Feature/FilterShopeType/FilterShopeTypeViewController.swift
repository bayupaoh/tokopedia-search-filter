//
//  FilterShopeTypeViewController.swift
//  tokopedia
//
//  Created by Bayu Paoh on 06/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

protocol FilterShopeTypeViewControllerProtocl {
    func onDismiss()
}

class FilterShopeTypeViewController: BaseViewController {

    let tableView: UITableView = {
        let tableView = UITableView(frame: .zero)
        tableView.register(FilterShopTypeCell.self, forCellReuseIdentifier: FilterShopTypeCell.identifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        return tableView
    }()
    
    let buttonApply: UIButton = {
        let button = UIButton(frame: .zero)
        button.backgroundColor = .green
        button.setTitle("Apply", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(applyButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private var filterShopTypeViewModel: FilterShopTypeViewModelProtocol!
    private let disposeBag = DisposeBag()
    var delegate: FilterShopeTypeViewControllerProtocl?
    
    // MARK: STATICS
    static func instantiate(delegate: FilterShopeTypeViewControllerProtocl) -> FilterShopeTypeViewController {
        let controller = FilterShopeTypeViewController()
        controller.delegate = delegate
        return controller
    }

    // MARK: OVERRIDES

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupNavigationBar()
        setupTableView()
        setupViewModel()
    }
    
    // MARK: IBACTIONS AND OBJCS
    
    @objc func applyButtonPressed(sender: UIButton!) {
        filterShopTypeViewModel.save()
        delegate?.onDismiss()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func resetButtonPressed(_ sender: UITapGestureRecognizer) {
        filterShopTypeViewModel.reset()
    }
    
    // MARK: METHODS
    private func setupView() {
        title = "Shop Type"
        
        view.backgroundColor = .systemBackground
        view.addSubview(tableView)
        view.addSubview(buttonApply)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -55),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            buttonApply.heightAnchor.constraint(equalToConstant: 50),
            buttonApply.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            buttonApply.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            buttonApply.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
        ])
    }
    
    private func setupNavigationBar() {
        self.navigationController?.navigationBar.backIndicatorImage = UIImage.imageClose
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage.imageClose
        
        let barButtonReset = UIBarButtonItem(title: "Reset", style: .done, target: self, action: #selector(resetButtonPressed))
        
        navigationItem.rightBarButtonItems = [barButtonReset]
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupViewModel() {
        
        filterShopTypeViewModel = FilterShopTypeViewModel()
        filterShopTypeViewModel.fetchShopTypes()
        
        filterShopTypeViewModel.shopTypes.drive(onNext: {[unowned self] (_) in
            self.tableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
}

// MARK: EXTENSIONS
extension FilterShopeTypeViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterShopTypeViewModel.numberOfShopType
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilterShopTypeCell.identifier, for: indexPath) as! FilterShopTypeCell
        if let viewModel = filterShopTypeViewModel?.viewModelForShopType(at: indexPath.row) {
            cell.configure(viewModel: viewModel)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        filterShopTypeViewModel.updateData(at: indexPath.row)
    }
}
