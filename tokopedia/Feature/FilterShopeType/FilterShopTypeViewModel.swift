//
//  FilterShopTypeViewModel.swift
//  tokopedia
//
//  Created by Bayu Paoh on 11/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class FilterShopTypeViewModel: FilterShopTypeViewModelProtocol {
    
    private var _shopTypesSource = ShopTypes.instance()
    private let _shopTypes = BehaviorRelay<[ShopType]>(value: ShopTypes.instance().datas)
    
    var shopTypes: Driver<[ShopType]> {
        return _shopTypes.asDriver()
    }
    
    var numberOfShopType: Int {
        return _shopTypes.value.count
    }
    
    init() {
        
    }
    
    func viewModelForShopType(at index: Int) -> FilterShopTypeCellViewModelProtocol? {
        guard index < _shopTypes.value.count else {
            return nil
        }
        return FilterShopTypeCellViewModel(shopType: _shopTypes.value[index])
    }
    
    func fetchShopTypes() {
        self._shopTypes.accept(_shopTypes.value)
    }
    
    func reset() {
        _shopTypesSource.reset()
        self._shopTypes.accept(_shopTypesSource.datas)
    }
    
    func updateData(at index: Int) {
        let data = _shopTypes.value
        data[index].isSelect = !data[index].isSelect
        self._shopTypes.accept(data)
    }
    
    func save() {
        _shopTypesSource.datas = self._shopTypes.value
    }
}
