//
//  FilterShopTypeCellViewModelProtocol.swift
//  tokopedia
//
//  Created by Bayu Paoh on 12/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

protocol FilterShopTypeCellViewModelProtocol {
    var id: Int { get }
    var title: String { get }
    var isChoose: Bool { get }
}
