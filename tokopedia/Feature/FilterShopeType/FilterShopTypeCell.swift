//
//  FilterShopTypeCell.swift
//  tokopedia
//
//  Created by Bayu Paoh on 06/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit

class FilterShopTypeCell: UITableViewCell {
    
    let imageViewCheckList: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "ic_uncheck")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .center
        return imageView
    }()
    
    let labelTitle: UILabel = {
        let label = UILabel(frame: .zero)
        label.text = "Gold Merchant"
        label.font = label.font.withSize(18)
        label.numberOfLines = 0
        label.textColor = .darkText
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let viewSeparator: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = .systemBackground
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var stackViewContainer: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.imageViewCheckList, self.labelTitle])
        stackView.axis = .horizontal
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 8
        return stackView
    }()
    
    // MARK: STATICS
    
    static var identifier: String = "FilterShopTypeCell"

    // MARK: OVERRIDES

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    // MARK: IBOUTLETS & OBJCS
    
    // MARK: METHODS
    private func setupView() {
        self.backgroundColor = .white
        self.addSubview(stackViewContainer)
        self.addSubview(viewSeparator)
        NSLayoutConstraint.activate([
            stackViewContainer.topAnchor.constraint(equalTo: self.topAnchor, constant: 16),
            stackViewContainer.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16),
            stackViewContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8),
            stackViewContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8),
            imageViewCheckList.widthAnchor.constraint(equalToConstant: 16),
            viewSeparator.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0),
            viewSeparator.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20),
            viewSeparator.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0),
            viewSeparator.heightAnchor.constraint(equalToConstant: 1),

        ])
    }
    
    func configure(viewModel: FilterShopTypeCellViewModelProtocol) {
        labelTitle.text = viewModel.title
        imageViewCheckList.image = viewModel.isChoose ? UIImage(named: "ic_check"): UIImage(named: "ic_uncheck")
    }
}
