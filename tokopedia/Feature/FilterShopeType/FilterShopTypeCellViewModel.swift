//
//  FilterShopTypeCellViewModel.swift
//  tokopedia
//
//  Created by Bayu Paoh on 12/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

class FilterShopTypeCellViewModel: FilterShopTypeCellViewModelProtocol {
    
    private var shopType: ShopType

    var id: Int {
        return shopType.id
    }

    var title: String {
        return shopType.title ?? ""
    }
    
    var isChoose: Bool {
        return shopType.isSelect
    }

    init(shopType: ShopType) {
        self.shopType = shopType
    }
}
