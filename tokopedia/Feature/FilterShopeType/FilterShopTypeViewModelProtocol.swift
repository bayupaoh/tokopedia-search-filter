//
//  FilterShopTypeViewModelProtocl.swift
//  tokopedia
//
//  Created by Bayu Paoh on 11/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol FilterShopTypeViewModelProtocol {

    var shopTypes: Driver<[ShopType]>{ get }
    var numberOfShopType: Int{ get }
    
    func viewModelForShopType(at index: Int) -> FilterShopTypeCellViewModelProtocol?
    func fetchShopTypes()
    func reset()
    func updateData(at index: Int)
    func save()
}
