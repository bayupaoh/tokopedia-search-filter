//
//  ProductCell.swift
//  tokopedia
//
//  Created by Bayu Paoh on 02/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit
import Kingfisher

class ProductCell: UICollectionViewCell {
    
    private let imageViewProduct: UIImageView = {
        let image = UIImageView(frame: .zero)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .gray
        return image
    }()
    
    private let labelTitle: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 2
        label.textColor = .black
        return label
    }()
    
    private let labelPrice: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .red
        return label
    }()
    
    private lazy var stackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.imageViewProduct, self.labelTitle, self.labelPrice])
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 8
        return stackView
    }()
    
    // MARK: STATICS
    
    static var identifier: String = "ProductCell"

    // MARK: OVERRIDES

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    // MARK: METHODS
    
    private func setupView() {
        backgroundColor = .white
        self.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            imageViewProduct.heightAnchor.constraint(equalToConstant: 125),
            stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 8),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16),
            stackView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8)
        ])
    }
    
    func configure(viewModel: ProductCellViewModelProtocol) {
        labelTitle.text = viewModel.name
        labelPrice.text = viewModel.price
        imageViewProduct.kf.setImage(with: viewModel.imageUrl)
    }
}
