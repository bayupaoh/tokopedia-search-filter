//
//  ViewController.swift
//  tokopedia
//
//  Created by Bayu Paoh on 30/11/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit
import Alamofire
import RxCocoa
import RxSwift

class MainViewController: BaseViewController {
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = Dimens.cellProduct
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(ProductCell.self, forCellWithReuseIdentifier: ProductCell.identifier)
        collectionView.register(LoadMoreCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: LoadMoreCollectionReusableView.identifier)
        collectionView.backgroundColor = .systemBackground
        return collectionView
    }()
    
    private let button: UIButton = {
        let button = UIButton(frame: .zero)
        button.backgroundColor = .green
        button.setTitle("Filter", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(filterButtonPressed), for: .touchUpInside)
        return button
    }()
    
    private var loadingView: LoadMoreCollectionReusableView?
    private var refreshControl = UIRefreshControl()
    private var mainViewModel: MainViewModelProtocol!
    private let disposeBag = DisposeBag()
    private var isLoading = false

    // MARK: STATICS
    
    static func instantiate() -> UINavigationController {
        let controller = MainViewController()
        let nav = UINavigationController(rootViewController: controller)
        nav.navigationBar.backgroundColor = .white
        nav.navigationBar.barTintColor = .white
        nav.navigationBar.tintColor = .gray
        return nav
    }
        
    // MARK: OVERRIDES
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Search"
        setupView()
        setupCollectionView()
        setupViewModel()
    }
    
    // MARK: IBACTIONS AND OBJCS
    
    @objc func filterButtonPressed(sender: UIButton!) {
        let controller = FilterViewController.instantiate(delegate: self)
        pushController(controller: controller)
    }
    
    @objc func refresh(sender: AnyObject) {
        mainViewModel.fetchMovie(isRefresh: true)
    }
    
    // MARK: METHODS
    
    private func setupView() {
        view.backgroundColor = .systemBackground
        view.addSubview(collectionView)
        view.addSubview(button)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -55),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8),
            button.heightAnchor.constraint(equalToConstant: 50),
            button.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            button.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
        ])
    }
    
    private func setupCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        collectionView.addSubview(refreshControl)
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
    private func setupViewModel() {
        mainViewModel = MainViewModel(searchProductUseCase: SearchProductUseCase(searchProductRepository: SearchProductRepository(networkService: NetworkService(sessionManager: SessionManager()))))
        
        mainViewModel.fetchMovie(isRefresh: true)
        
        mainViewModel.products.drive(onNext: {[unowned self] (_) in
            self.isLoading = false
            self.collectionView.reloadData()
        }).disposed(by: disposeBag)
        
        mainViewModel.isFetching.drive(refreshControl.rx.isRefreshing)
            .disposed(by: disposeBag)
                
        mainViewModel.error.drive(onNext: {[unowned self] (error) in
            if self.mainViewModel.hasError {
                guard let message = error else {
                    self.showAlert(message: "Ooopss... Something wrong")
                    return
                }
                self.showAlert(message: message)
            }
        }).disposed(by: disposeBag)
        
        collectionView.rxReachedBottom.asObservable().subscribe({_ in
            self.isLoading = true
            self.mainViewModel.fetchMovie(isRefresh: false)
        }).disposed(by: disposeBag)
    }
    
    private func showAlert(message: String) {
        let alert = createAlert(title: "Error", message: message)
        present(alert, animated: true, completion: nil)
    }
}

// MARK: EXTENSIONS

extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return mainViewModel.numberOfProducts
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProductCell.identifier, for: indexPath) as! ProductCell
        if let viewModel = mainViewModel?.viewModelForProduct(at: indexPath.row) {
            cell.configure(viewModel: viewModel)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if self.isLoading {
            return CGSize.zero
        } else {
            return CGSize(width: collectionView.bounds.size.width, height: 55)
        }
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let aFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: LoadMoreCollectionReusableView.identifier, for: indexPath) as! LoadMoreCollectionReusableView
            loadingView = aFooterView
            loadingView?.backgroundColor = UIColor.clear
            return aFooterView
        }
        return UICollectionReusableView()
    }

    func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingView?.activityIndicator.startAnimating()
        }
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingView?.activityIndicator.stopAnimating()
        }
    }
}

extension MainViewController: FilterViewControllerProtocol {
    func onDismiss() {
        mainViewModel.fetchMovie(isRefresh: true)
    }
}
