//
//  ProductCellViewModel.swift
//  tokopedia
//
//  Created by Bayu Paoh on 10/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

struct ProductCellViewModel: ProductCellViewModelProtocol{
    
    private var product: Product
    
    init(product: Product) {
        self.product = product
    }
    
    var name: String {
        return product.name ?? ""
    }
    
    var price: String {
        return product.price ?? ""
    }
    
    var imageUrl: URL {
        return URL(string: product.imageUri ?? "")!
    }
}
