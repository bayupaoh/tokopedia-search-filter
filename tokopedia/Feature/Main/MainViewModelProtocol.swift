//
//  MainViewModelProtocol.swift
//  tokopedia
//
//  Created by Bayu Paoh on 11/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol MainViewModelProtocol {

    var isFetching: Driver<Bool>{ get }
    var products: Driver<[Product]>{ get }
    var error: Driver<String?>{ get }
    var hasError: Bool{ get }
    var numberOfProducts: Int{ get }
    
    func viewModelForProduct(at index: Int) -> ProductCellViewModel?
    
    func fetchMovie(isRefresh: Bool)
}
