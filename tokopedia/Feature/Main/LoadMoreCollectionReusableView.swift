//
//  LoadMoreCollectionReusableView.swift
//  tokopedia
//
//  Created by Bayu Paoh on 12/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit

class LoadMoreCollectionReusableView: UICollectionReusableView {
    
    let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        return activityIndicator
    }()
    
    // MARK: STATICS
    
    static var identifier: String = "LoadMoreCollectionReusableView"

    // MARK: OVERRIDES

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: IBACTIONS AND OBJCS
    
    // MARK: METHODS

    private func setupView() {
        backgroundColor = .clear
        self.addSubview(activityIndicator)
        
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
    }
}
