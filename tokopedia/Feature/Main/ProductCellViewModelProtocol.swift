//
//  ProductCellViewModelProtocol.swift
//  tokopedia
//
//  Created by Bayu Paoh on 11/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

protocol ProductCellViewModelProtocol {

    var name: String { get }
    var price: String { get }
    var imageUrl: URL { get }
}
