//
//  MainViewModel.swift
//  tokopedia
//
//  Created by Bayu Paoh on 10/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class MainViewModel: MainViewModelProtocol {
    
    private var searchProductUseCase: SearchProductUseCaseProtocol
    
    private let disposeBag = DisposeBag()
    private var page = 0
    private let _products = BehaviorRelay<[Product]>(value: [])
    private let _isFetching = BehaviorRelay<Bool>(value: false)
    private let _error = BehaviorRelay<String?>(value: nil)
    
    private let jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        return jsonDecoder
    }()
    
    var isFetching: Driver<Bool> {
        return _isFetching.asDriver()
    }
    
    var products: Driver<[Product]> {
        return _products.asDriver()
    }
    
    var error: Driver<String?> {
        return _error.asDriver()
    }
    
    var hasError: Bool {
        return _error.value != nil
    }
    
    var numberOfProducts: Int {
        return _products.value.count
    }
    
    init(searchProductUseCase: SearchProductUseCaseProtocol) {
        self.searchProductUseCase = searchProductUseCase
    }
    
    func viewModelForProduct(at index: Int) -> ProductCellViewModel? {
        guard index < _products.value.count else {
            return nil
        }
        return ProductCellViewModel(product: _products.value[index])
    }
    
    func fetchMovie(isRefresh: Bool) {
        let filterData = FilterProduct.instance()
        if isRefresh {
            self._products.accept([])
            self._isFetching.accept(true)
            page = 0
        }
        var parameters: [String: Any] = [
            "q": "samsung",
            "pmin": filterData.priceMin,
            "pmax": filterData.priceMax,
            "wholesale": filterData.wholeSale,
            "official": filterData.official,
            "start": page,
            "rows": 10,
        ]

        if filterData.fshop == "2" {
            parameters["fshop"] = filterData.fshop
        }

        searchProductUseCase.searchProduct(parameters: parameters)
            .do(onNext: { datas in
                self._isFetching.accept(false)
                if isRefresh {
                    self._products.accept(datas)
                } else {
                    var products = self._products.value
                    products.append(contentsOf: datas)
                    self._products.accept(products)
                }
                self.page += 1
            })
            .do(onError: { error in
                self._isFetching.accept(false)
                self._error.accept(error.localizedDescription)
            })
        .subscribe()
        .disposed(by: disposeBag)
    }
}
