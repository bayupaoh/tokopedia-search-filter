//
//  FilterViewController.swift
//  tokopedia
//
//  Created by Bayu Paoh on 04/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol FilterViewControllerProtocol {
    func onDismiss()
}

class FilterViewController: BaseViewController {
    
    @IBOutlet weak private var collectionViewShopTypeFilter: UICollectionView!
    @IBOutlet weak private var stackViewShopType: UIStackView!
    @IBOutlet weak private var viewSlider: UIView!
    @IBOutlet weak private var labelLowerPrice: UILabel!
    @IBOutlet weak private var labelHigherPrice: UILabel!
    @IBOutlet weak private var switchWholeSale: UISwitch!
    
    let button: UIButton = {
        let button = UIButton(frame: .zero)
        button.backgroundColor = .green
        button.setTitle("Apply", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(applyButtonPressed), for: .touchUpInside)
        return button
    }()
    
    let slidePrice: RangeSlider = {
        let rangeSlider = RangeSlider(frame: .zero)
        rangeSlider.trackHighlightTintColor = .green
        rangeSlider.translatesAutoresizingMaskIntoConstraints = false
        rangeSlider.lowerValue = 0
        rangeSlider.upperValue = 1.0
        return rangeSlider
    }()
    
    var delegate: FilterViewControllerProtocol?
    var filterViewModel: FilterViewModel!
    private let disposeBag = DisposeBag()
    
    // MARK: STATICS
    static func instantiate(delegate: FilterViewControllerProtocol) -> FilterViewController {
        let controller = FilterViewController(nibName: self.className(), bundle: nil)
        controller.delegate = delegate
        return controller
    }
    
    // MARK: OVERRIDES
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSlider()
        setupView()
        setupNavigationBar()
        setupCollectionView()
        setupActionView()
        setupViewModel()
    }

    // MARK: IBACTIONS AND OBJCS
    
    @objc func applyButtonPressed(sender: UIButton!) {
        filterViewModel.setFilerProduct(scrollMin: slidePrice.lowerValue, scrollMax: slidePrice.upperValue, wholeSale: switchWholeSale.isOn)
        delegate?.onDismiss()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func resetButtonPressed(_ sender: UITapGestureRecognizer) {
        filterViewModel.reset()
    }

    @objc func filterShopTypePressed(_ sender: UITapGestureRecognizer) {
        let controller = FilterShopeTypeViewController.instantiate(delegate: self)
        pushController(controller: controller)
    }

    @objc func rangeSliderValueChanged(_ rangeSlider: RangeSlider) {
        labelLowerPrice.text = ProductHelper.priceWithoutCurrency(slider: rangeSlider.lowerValue).convertToCurency()
        labelHigherPrice.text = ProductHelper.priceWithoutCurrency(slider: rangeSlider.upperValue).convertToCurency()
    }
    
    // MARK: METHODS
    private func setupView() {
        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.tintColor = .gray
        title = "Filter"
        
        view.addSubview(button)
        NSLayoutConstraint.activate([
            button.heightAnchor.constraint(equalToConstant: 50),
            button.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            button.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            button.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            slidePrice.topAnchor.constraint(equalTo: viewSlider.topAnchor, constant: 0),
            slidePrice.leadingAnchor.constraint(equalTo: viewSlider.leadingAnchor, constant: 0),
            slidePrice.trailingAnchor.constraint(equalTo: viewSlider.trailingAnchor, constant: 0),
            slidePrice.heightAnchor.constraint(equalToConstant: 35),
        ])
    }
    
    private func setupNavigationBar() {
        self.navigationController?.navigationBar.backIndicatorImage = UIImage.imageClose
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage.imageClose
        
        let barButtonReset = UIBarButtonItem(title: "Reset", style: .done, target: self, action: #selector(resetButtonPressed))
        
        navigationItem.rightBarButtonItems = [barButtonReset]
    }
    
    private func setupCollectionView() {
        let layoutShopTypeFilter = UICollectionViewFlowLayout()
        layoutShopTypeFilter.scrollDirection = .horizontal
        layoutShopTypeFilter.itemSize = Dimens.shopeTypeFilter
        collectionViewShopTypeFilter.setCollectionViewLayout(layoutShopTypeFilter, animated: true)
        collectionViewShopTypeFilter.register(ShopTypeCell.self, forCellWithReuseIdentifier: ShopTypeCell.identifier)
        collectionViewShopTypeFilter.dataSource = self
        collectionViewShopTypeFilter.delegate = self
    }
    
    private func setupSlider() {
        viewSlider.addSubview(slidePrice)
        slidePrice.addTarget(self, action: #selector(rangeSliderValueChanged), for: .valueChanged)
    }

    private func setupActionView() {
        stackViewShopType.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filterShopTypePressed)))
    }
    
    private func setupViewModel() {
        filterViewModel = FilterViewModel()
        filterViewModel.fetchShopTypes()

        filterViewModel.filterProduct.drive(onNext: {[unowned self] (data) in
            self.slidePrice.lowerValue = data.scrollMin
            self.slidePrice.upperValue = data.scrollMax
            self.switchWholeSale.isOn = data.wholeSale
            self.labelLowerPrice.text = ProductHelper.priceWithoutCurrency(slider: self.slidePrice.lowerValue).convertToCurency()
            self.labelHigherPrice.text = ProductHelper.priceWithoutCurrency(slider: self.slidePrice.upperValue).convertToCurency()
        }).disposed(by: disposeBag)
        
        filterViewModel.shopTypes.drive(onNext: {[unowned self] (_) in
            self.collectionViewShopTypeFilter.reloadData()
        }).disposed(by: disposeBag)
    }
}

// MARK: EXTENSIONS
extension FilterViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterViewModel.numberOfShopType
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ShopTypeCell.identifier, for: indexPath) as! ShopTypeCell
        cell.delegate = self
        if let viewModel = filterViewModel?.viewModelForShopType(at: indexPath.row) {
            cell.configure(viewModel: viewModel)
        }
        return cell
    }
}

extension FilterViewController: ShopTypeCellProtocol{
    func pressedDelete(id: Int) {
        filterViewModel.updateDataShopType(at: id)
    }
}

extension FilterViewController: FilterShopeTypeViewControllerProtocl{
    func onDismiss() {
        filterViewModel.fetchShopTypes()
    }
}
