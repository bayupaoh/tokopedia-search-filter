//
//  ShopTypeCell.swift
//  tokopedia
//
//  Created by Bayu Paoh on 05/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit

protocol ShopTypeCellProtocol {
    func pressedDelete(id: Int)
}

class ShopTypeCell: UICollectionViewCell {
    
    private let imageViewClose: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.image = UIImage(named: "ic_close")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private let viewClose: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = .systemBackground
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.cornerRadius = 18.0
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = true
        return view
    }()
    
    private let labelName: UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = .gray
        label.text = "Merchant Store"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let viewContainer: UIView = {
        let view = UIView(frame: .zero)
        view.backgroundColor = .white
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.gray.cgColor
        view.layer.cornerRadius = 18.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let view: UIView = {
        return UIView(frame: .zero)
    }()

    private lazy var stackView: UIStackView = { [unowned self] in
        let stackView = UIStackView(arrangedSubviews: [self.view, self.labelName, self.viewClose])
        stackView.axis = .horizontal
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 8
        return stackView
    }()

    var delegate: ShopTypeCellProtocol?
    var id: Int = -1

    // MARK: STATICS
    
    static var identifier: String = "ShopTypeCell"

    // MARK: OVERRIDES

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
        setupActionView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    // MARK: IBOUTLETS & OBJCS
    
    @objc func removeItemPressed(_ sender: UITapGestureRecognizer) {
        delegate?.pressedDelete(id: self.id)
    }

    // MARK: METHODS
    
    private func setupView() {
        backgroundColor = .white
        self.addSubview(viewContainer)
        self.viewContainer.addSubview(stackView)
        self.viewClose.addSubview(imageViewClose)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: self.topAnchor, constant: 1),
            stackView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1),
            stackView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 1),
            stackView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -1),
            stackView.topAnchor.constraint(equalTo: viewContainer.topAnchor, constant: 0),
            stackView.bottomAnchor.constraint(equalTo: viewContainer.bottomAnchor, constant: 0),
            stackView.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor, constant: 0),
            stackView.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor, constant: 0),
            viewClose.widthAnchor.constraint(equalToConstant: 37),
            view.widthAnchor.constraint(equalToConstant: 8),
            imageViewClose.topAnchor.constraint(equalTo: viewClose.topAnchor, constant: 12),
            imageViewClose.bottomAnchor.constraint(equalTo: viewClose.bottomAnchor, constant: -12),
            imageViewClose.leadingAnchor.constraint(equalTo: viewClose.leadingAnchor, constant: 12),
            imageViewClose.trailingAnchor.constraint(equalTo: viewClose.trailingAnchor, constant: -12)
        ])
    }
    
    func setupActionView() {
        viewClose.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(removeItemPressed)))
    }
    
    func configure(viewModel: FilterShopTypeCellViewModelProtocol) {
        self.id = viewModel.id
        labelName.text = viewModel.title
    }
}
