//
//  FilterViewModelProtocol.swift
//  tokopedia
//
//  Created by Bayu Paoh on 12/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol FilterViewModelProtocol {
    var shopTypes: Driver<[ShopType]>{ get }
    var filterProduct: Driver<FilterProduct>{ get }
    var numberOfShopType: Int{ get }
    
    func viewModelForShopType(at index: Int) -> FilterShopTypeCellViewModelProtocol?
    func fetchShopTypes()
    func reset()
    func updateDataShopType(at index: Int)
    func setFilerProduct(scrollMin: Double, scrollMax: Double, wholeSale: Bool)
}
