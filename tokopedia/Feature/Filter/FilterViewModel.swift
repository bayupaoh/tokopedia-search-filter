//
//  FilterViewModel.swift
//  tokopedia
//
//  Created by Bayu Paoh on 12/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class FilterViewModel: FilterViewModelProtocol {

    private var _shopTypeSource = ShopTypes.instance()
    private let _filterProductSource = FilterProduct.instance()
    private let _shopTypes = BehaviorRelay<[ShopType]>(value: [])
    private let _filterProduct = BehaviorRelay<FilterProduct>(value: FilterProduct.instance())
    
    var shopTypes: Driver<[ShopType]> {
        return _shopTypes.asDriver()
    }
    
    var filterProduct: Driver<FilterProduct> {
        return _filterProduct.asDriver()
    }
    
    var numberOfShopType: Int {
        return _shopTypes.value.count
    }
    
    func viewModelForShopType(at index: Int) -> FilterShopTypeCellViewModelProtocol? {
        guard index < _shopTypes.value.count else {
            return nil
        }
        return FilterShopTypeCellViewModel(shopType: _shopTypes.value[index])
    }
    
    func fetchShopTypes() {
        let shopTypes = _shopTypeSource.datas.filter{ $0.isSelect }
        self._shopTypes.accept(shopTypes)
    }
    
    func reset() {
        _shopTypeSource.reset()
        _filterProductSource.reset()
        let data = _shopTypeSource.datas.filter{ $0.isSelect }
        self._shopTypes.accept(data)
        self._filterProduct.accept(_filterProductSource)
    }
    
    func updateDataShopType(at index: Int) {
        let data = self._shopTypes.value
        data.map {
            if $0.id == index {
                $0.isSelect = !$0.isSelect
            }
        }
        self._shopTypes.accept(data.filter{ $0.isSelect })
    }
    
    func setFilerProduct(scrollMin: Double, scrollMax: Double, wholeSale: Bool) {
        let fShop = _shopTypeSource.datas.first?.isSelect ?? false ? "2" : "0"
        let isOfficial = _shopTypeSource.datas.last?.isSelect ?? false
        _filterProductSource.setFilter(scrollMin: scrollMin, scrollMax: scrollMax, wholeSale: wholeSale, official: isOfficial, fshop: fShop)
    }
}
