//
//  NetworkHelper.swift
//  tokopedia
//
//  Created by Bayu Paoh on 13/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

class NetworkHelper {
    
    static func customError(code: Int, message: String) -> Error {
        return NSError(domain: "Tokopedia Search", code: code, userInfo: [NSLocalizedDescriptionKey: message])
    }
}
