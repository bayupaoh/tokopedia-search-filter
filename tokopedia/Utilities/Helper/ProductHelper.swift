//
//  ProductHelper.swift
//  tokopedia
//
//  Created by Bayu Paoh on 12/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

class ProductHelper {
    
    static let higherPrice = 30000000.0

    static func priceWithoutCurrency(slider: Double) -> String {
        var priceInt = Int((slider * higherPrice))
        priceInt = priceInt - (priceInt % 1000)
        return String(priceInt)
    }

    static func priceWithoutCurrency(slider: Double) -> Int {
        var priceInt = Int((slider * higherPrice))
        priceInt = priceInt - (priceInt % 1000)
        return priceInt
    }
}
