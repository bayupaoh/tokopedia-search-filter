//
//  UIViewControllerExtension.swift
//  tokopedia
//
//  Created by Bayu Paoh on 04/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit

extension UIViewController {
    
    static func className() -> String {
        return String(describing: self)
    }
    
    func pushController(controller: UIViewController, withbackTitle backTitle: String? = "", animated: Bool = true, color: UIColor = .gray) {
        let backItem = UIBarButtonItem()
        backItem.tintColor = color
        backItem.title = "  \(backTitle ?? "")"
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller, animated: animated)
    }
    
    func createAlert(title: String? = nil, message: String? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
}
