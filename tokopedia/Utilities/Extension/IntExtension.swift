//
//  DoubleExtension.swift
//  tokopedia
//
//  Created by Bayu Paoh on 07/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

extension Int {
    func convertToCurency(identifier: String = "id_ID") -> String {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: identifier)
        formatter.numberStyle = .currency
        if let currency = formatter.string(from: self as NSNumber) {
            return currency
        }
        return ""
    }
}
