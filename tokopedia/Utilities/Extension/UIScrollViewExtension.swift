//
//  UIScrollViewExtension.swift
//  tokopedia
//
//  Created by Bayu Paoh on 09/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension UIScrollView {
    public var rxReachedBottom: Observable<Void> {
        return rx.contentOffset
            .debounce(RxTimeInterval.milliseconds(25), scheduler: MainScheduler.instance)
            .flatMap { [weak self] contentOffset -> Observable<Void> in
                guard let scrollView = self else {
                    return Observable.empty()
                }
                
                let visibleHeight = scrollView.frame.height - scrollView.contentInset.top - scrollView.contentInset.bottom
                let yAxis = contentOffset.y + scrollView.contentInset.top
                let threshold = max(0.0, scrollView.contentSize.height - visibleHeight)
                
                return yAxis >= threshold ? Observable.just(()) : Observable.empty()
        }
    }
}
