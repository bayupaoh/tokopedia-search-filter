//
//  SearchProductRepositoryProtocol.swift
//  tokopedia
//
//  Created by Bayu Paoh on 11/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol SearchProductRepositoryProtocol {
    func searchProduct(parameters: [String: Any]?) -> Observable<Products>
}
