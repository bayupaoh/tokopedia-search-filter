//
//  SearchProductRepository.swift
//  tokopedia
//
//  Created by Bayu Paoh on 11/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class SearchProductRepository: SearchProductRepositoryProtocol {
    
    private var networkService: NetworkingProtocol
    private let disposeBag = DisposeBag()
    private let jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        return jsonDecoder
    }()
    
    init(networkService: NetworkingProtocol) {
        self.networkService = networkService
    }
    
    func searchProduct(parameters: [String: Any]?) -> Observable<Products> {
        return Observable.create{ observer -> Disposable in
            self.networkService.get(path: "/search/v2.5/product", parameters: parameters, headers: nil) { (data, error) in
                if let response = data {
                    do {
                        let product = try self.jsonDecoder.decode(Products.self, from: response)
                            observer.onNext(product)
                    } catch {
                        observer.onError(error)
                    }
                } else {
                    if let error = error {
                        observer.onError(error)
                    }
                }
            }
            return Disposables.create()
        }
    }
}
