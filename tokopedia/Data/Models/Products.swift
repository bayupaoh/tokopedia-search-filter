//
//  Product.swift
//  tokopedia
//
//  Created by Bayu Paoh on 09/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

public struct Products: Codable {
    let status: Status
    let data: [Product]
}

public struct Status: Codable {
    let errorCode: Int
    let message: String?
    
    private enum CodingKeys: String, CodingKey{
        case errorCode = "error_code"
        case message
    }
}

public struct Product: Codable {
    let id: Int
    let name: String?
    let imageUri: String?
    let price: String?
    
    private enum CodingKeys: String, CodingKey{
        case id
        case name
        case imageUri = "image_uri"
        case price
    }
}
