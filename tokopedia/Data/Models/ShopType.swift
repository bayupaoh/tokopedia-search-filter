//
//  ShopType.swift
//  tokopedia
//
//  Created by Bayu Paoh on 11/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

public class ShopType {
    
    var id: Int
    var title: String?
    var isSelect: Bool
    
    init(id: Int, title: String, isSelect: Bool) {
        self.id = id
        self.title = title
        self.isSelect = isSelect
    }
}

public class ShopTypes {
    
    private static var shopTypes = ShopTypes()
    static func instance() -> ShopTypes {
        return shopTypes
    }
    
    var datas: [ShopType] = [ShopType(id: 0, title: "Gold Merchant", isSelect: true), ShopType(id: 1, title: "Official Store", isSelect: true)]

    init() {
    }
    
    func reset() {
        datas = [ShopType(id: 0, title: "Gold Merchant", isSelect: true), ShopType(id: 1, title: "Official Store", isSelect: true)]
    }
}
