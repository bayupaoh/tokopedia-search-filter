//
//  FilterProduct.swift
//  tokopedia
//
//  Created by Bayu Paoh on 12/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

class FilterProduct {
    
    var wholeSale: Bool = true
    var official: Bool = true
    var fshop: String = "2"
    var scrollMin: Double = 0.0
    var scrollMax: Double = 1.0
    var priceMin: String = ProductHelper.priceWithoutCurrency(slider: 0.0)
    var priceMax: String = ProductHelper.priceWithoutCurrency(slider: 1.0)

    private static let filterProduct = FilterProduct()
    
    static func instance() -> FilterProduct {
         return filterProduct
    }
    
    func setFilter(scrollMin: Double, scrollMax: Double, wholeSale: Bool, official: Bool, fshop: String) {

        self.wholeSale = wholeSale
        self.official = official
        self.fshop = fshop
        self.scrollMin = scrollMin
        self.scrollMax = scrollMax
        self.priceMin = ProductHelper.priceWithoutCurrency(slider: self.scrollMin)
        self.priceMax = ProductHelper.priceWithoutCurrency(slider: self.scrollMax)
    }
    
    func reset() {
        wholeSale = true
        official = true
        fshop = "2"
        scrollMin = 0.0
        scrollMax = 1.0
        priceMin = ""
        priceMax = ""
    }
}
