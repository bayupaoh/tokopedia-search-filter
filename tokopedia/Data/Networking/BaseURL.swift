//
//  BaseURL.swift
//  tokopedia
//
//  Created by Bayu Paoh on 09/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

enum NetworkEnvironment {
    case dev
    case production
}

open class BaseURL: NSObject {
    
    public static var url: URLComponents {
        switch stage {
        case .dev:
            var urlComponents = URLComponents()
            urlComponents.scheme = "https"
            urlComponents.host = "ace.tokopedia.com"
            return urlComponents
        case .production:
            var urlComponents = URLComponents()
            urlComponents.scheme = "https"
            urlComponents.host = "ace.tokopedia.com"
            return urlComponents
        }
    }
    
    static let stage: NetworkEnvironment = .dev
}
