//
//  NetworkingProtocol.swift
//  tokopedia
//
//  Created by Bayu Paoh on 09/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

protocol NetworkingProtocol {
    func get(path: String, parameters: [String: Any]?, headers: [String: String]?, completion: @escaping (_ result: Data?, _ error: Error?) -> Void)
}
