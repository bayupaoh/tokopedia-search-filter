//
//  NetworkService.swift
//  tokopedia
//
//  Created by Bayu Paoh on 09/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import Alamofire

class NetworkService: NetworkingProtocol {
    
    var sessionManager: SessionManager?

    init(sessionManager: SessionManager) {
        self.sessionManager = sessionManager
    }
    
    func get(path: String, parameters: [String: Any]?, headers: [String: String]?, completion: @escaping (Data?, Error?) -> Void) {
        
        var urlComponents = BaseURL.url
        urlComponents.path = path
        
        let url = urlComponents.url?.absoluteString ?? ""

        sessionManager?.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers).responseJSON { response in
            switch response.result {
            case .success(_):
                guard let data = response.data else {
                    completion(nil, NetworkHelper.customError(code: 401, message: "Ooops.. Something wrong"))
                    return
                }
                completion(data, nil)
                
            case .failure(_):
                completion(nil, response.result.error)
            }
        }
    }
}
