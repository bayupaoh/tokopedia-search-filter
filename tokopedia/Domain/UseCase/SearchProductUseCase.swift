//
//  SearchProductUseCase.swift
//  tokopedia
//
//  Created by Bayu Paoh on 11/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class SearchProductUseCase: SearchProductUseCaseProtocol {
    
    private var searchProductRepository: SearchProductRepositoryProtocol
    
    init(searchProductRepository: SearchProductRepositoryProtocol) {
        self.searchProductRepository = searchProductRepository
    }
    
    func searchProduct(parameters: [String: Any]?) -> Observable<[Product]> {
        return searchProductRepository.searchProduct(parameters: parameters).map { $0.data }
    }
}
