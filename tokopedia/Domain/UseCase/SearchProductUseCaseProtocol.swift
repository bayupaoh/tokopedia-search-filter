//
//  SearchProductUseCaseProtocol.swift
//  tokopedia
//
//  Created by Bayu Paoh on 11/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol SearchProductUseCaseProtocol {
    func searchProduct(parameters: [String: Any]?) -> Observable<[Product]>
}
