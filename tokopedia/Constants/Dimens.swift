//
//  Dimens.swift
//  tokopedia
//
//  Created by Bayu Paoh on 03/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit

struct Dimens {
    static let cellProduct = CGSize(width: (UIScreen.main.bounds.width - 28) / 2, height: 225.0)
    static let shopeTypeFilter = CGSize(width: 150.0, height: 40.0)
}
