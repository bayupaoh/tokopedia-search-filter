//
//  Colors.swift
//  tokopedia
//
//  Created by Bayu Paoh on 03/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit

extension UIColor {
    static let green = UIColor(displayP3Red: 65 / 250, green: 180 / 250, blue: 73 / 250, alpha: 1.0)
    static let red = UIColor(displayP3Red: 250 / 250, green: 90 / 250, blue: 30 / 250, alpha: 1.0)
    static let gray = UIColor(displayP3Red: 138 / 250, green: 138 / 250, blue: 138 / 250, alpha: 1.0)
    static let grayLight = UIColor(displayP3Red: 248 / 250, green: 248 / 250, blue: 248 / 250, alpha: 1.0)
    static let systemBackground = UIColor(displayP3Red: 241 / 250, green: 241 / 250, blue: 241 / 250, alpha: 1.0)
}
