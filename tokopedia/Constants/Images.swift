//
//  Images.swift
//  tokopedia
//
//  Created by Bayu Paoh on 04/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit

extension UIImage {
    static let imageClose = UIImage(named: "ic_close")
}
