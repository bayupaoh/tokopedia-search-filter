//
//  ViewModelPRotocol.swift
//  tokopedia
//
//  Created by Bayu Paoh on 09/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import Foundation

protocol ViewModelProtocol {
    associatedtype Input
    associatedtype Output
    
    func transform(fromInput input: Input) -> Output
}
