//
//  BaseViewController.swift
//  tokopedia
//
//  Created by Bayu Paoh on 07/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
}
