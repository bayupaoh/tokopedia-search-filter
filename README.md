# tokopedia-search-filter
###### Bayu Firmawan Paoh - Software Engineer iOS
Mini Project - Search Filter Repository for tokopedia screening test

### Library
* [Alamofire](https://github.com/Alamofire/Alamofire) - Elegent networking for swift
* [RXSwift](https://github.com/ReactiveX/RxSwift) - ReactiveX for Swift
* [KingFisher](https://github.com/onevcat/Kingfisher) - Download and caching image.
* [SwiftLint](https://github.com/realm/SwiftLint) - Swift style and conventions

### Demo
![demo](./demo/demo.gif)

