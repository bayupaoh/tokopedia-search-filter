//
//  ProductHelperTest.swift
//  tokopediaTests
//
//  Created by Bayu Paoh on 13/12/19.
//  Copyright © 2019 Bayu Paoh. All rights reserved.
//

import XCTest

@testable import tokopedia

class ProductHelperTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPriceWithoutCurrencyInt() {
        let harga: Int = ProductHelper.priceWithoutCurrency(slider: 1)
        
        XCTAssertEqual(harga, 30000000)
    }

    func testPriceWithoutCurrencyString() {
        let harga: String = ProductHelper.priceWithoutCurrency(slider: 1)
        
        XCTAssertEqual(harga, "30000000")
    }
}
